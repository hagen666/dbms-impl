# A trial of the DBMS Implementation

## Storage Managemement

Now, we almost implemented the storage management.
It mainly includes three parts:<br>
<ul>
<li>Storage Design
<li>Buffer Management
<li>Segment Design
</ul>
### 1.Storage Design

The size of one page in our experiment is 1024*8 Bytes.<br>
One page include three parts:<br>
1. ** page header(0x0000-0x0003) **<br>
&nbsp;&nbsp; usedByte(2 Bytes): how many bytes are used in the page?<br>
&nbsp;&nbsp; userFlag(2 Bytes): 16bits to store some information.<br>
2. ** page control(0x0004-0x0393) **<br>
&nbsp;&nbsp; A bit map(909byte): each bit denote whether one byte is used.<br>
&nbsp;&nbsp; Byte aligment(3byte): 3 empty bytes to make the header alignment.<br>
3. ** page data(0x0394-0x1fff) **<br>

#### Core System Call
pread, pwrite, open, close.

#### How to?
We only give an example when the page used for writing data in the data segment.
```
        Page* page = frame->page;
        PageUtil* util = manager->getPageUtil();
        PageAddr offset = util->allocSpace(*page,tupleSize);
        if(offset<=0)
        {
            // no space left on this page.
        }
        else
        {
            tuple->offset= offset;
            tuple->tupleAddr = addr+offset;
            short written = TableUtil::writeTuple(page->data+offset,tuple,meta);
            // to do sth
        }
```


### Main Directory
/storage

### 2.Buffer Management
The class BufferManager was built to manage buffer. It is called when segments request to invoke page for reading or writting.
#### Core System Function & Structure
memcpy, pthread_create, std::map, std::queue, std::set.
#### Main Directory
/buffer

#### How to?
When read:
```
 BufferFrame* frame = manager->requestPageForRead(addr);
 Page* page = frame->page;
 Byte* data = page->data;
 //read sth from page->data
 manager->finishRead(frame);
```
When write:
```
 BufferFrame* frame = manager->requestPageForWrite(addr,false);
 Page* page = frame->page;
 PageUtil* util = manager->getPageUtil();
 // edit data use PageUtil、TableUtil or DataUtil and so on.
 directorySegment->updateSpaceMark(frame,segmentType,pageStatus); // if needed
 frame->edit = true; // if page was edited.
 manager->finishWrite(frame);
```
If a new page is needed, we can only get it from directory segment by:
```
Addr addr = directorySegment->allocateBlock(segmentType,pageStatus);
// and then we can get it from the bufferManager.
BufferFrame* frame = manager->requestPageForWrite(addr,false);
// and do sth interesting.
```
The following statement is only invoked when the directory segment creates a new directory page, and should not be invoke by others:
```
BufferFrame* frame = manager->allocNewBufferFrameForWrite();
```

### 3.Segment Design
Three segments were implemented now, including:
<br>
1.Directory Segment<br>
&nbsp;&nbsp; All of the pages are managed by this segment.<br>
2.Metadata Segment<br>
&nbsp;&nbsp; It only includes one page, to store the metadata.<br>
3.Data Segment<br>
&nbsp;&nbsp; This segment can store data(a sequence of tuples).<br>

For more details of the segment, we will put them on wiki page.